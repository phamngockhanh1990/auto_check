require 'rubygems'
require "selenium-webdriver"
require 'logger'
require 'pry'
require 'yaml'


# load all ruby file in folder tasks
Dir[File.dirname(__FILE__) + '/tasks/*.rb'].each {|file| require file }

if ARGV[0]
  Object.const_get(ARGV[0].capitalize).new.run! rescue p "Task not exist!"
else
  p 'Please choose name of task. Ex: `ruby run.rb task_name`'
end
