class Aipo
  attr_reader :config, :url, :username, :password, :driver, :logger

  def initialize
    @config = YAML.load_file(File.dirname(__FILE__) + '/../config/config.yml')['aipo']
    @url = @config['url']
    @username = @config['acccount']['user']
    @password = @config['acccount']['pass']
    @logger = Logger.new(File.dirname(__FILE__) + '/../log/aipo.log')
    logger.info "=== Start aipo task at: #{Time.now} ==="
  end

  def run!
    # TODO if time  ==> checking; else => talkin
    sleep_random
    start_server
    auto_checkin # Example
  rescue => e
    logger.error e
  ensure
    logger.info "=== Finish aipo task at: #{Time.now} ==="
  end

  def start_server
    @driver = Selenium::WebDriver.for @config['web_driver'].to_sym
  end

  def auto_checkin
    logger.info " - Start auto checking"
    auto_login
    driver.find_element(:xpath, "//td[@class='w25 center']//input[@class='auiButtonAction']").click
    auto_quit
    logger.info " - Finish auto checking"
  end

  def auto_talkin
    logger.info " - Start auto talkin"
    auto_login
    sleep(4)
    driver.find_element(:xpath, "//td[@class='w25 center']//input[@class='auiButtonAction']").click

    auto_quit
    logger.info " - Finish auto talkin"
  end

  def auto_login
    driver.navigate.to url
    element = driver.find_element(:name, 'member_username')
    element.send_keys username
    element = driver.find_element(:name, 'password')
    element.send_keys password

    element.submit
  end

  def auto_quit
    driver.quit
  end

  def sleep_random
    sleep(rand(60*2..60*8))
  end

end
